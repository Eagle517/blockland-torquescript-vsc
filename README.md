# Blockland TorqueScript VSC

TorqueScript syntax highlighting and code snippets tailored for Blockland.

This extension is based on [zfbx's extension](https://github.com/zfbx/TorqueScript).

## Features
* Syntax highlighting for .cs, .gui, .mis, .cs.dasm, and .cs.deco files.
* Additional highlighting for:
	* Torque ML tags
	* All engine methods
* Code snippets/autocomplete
	* Templates for common structures
	* Torque ML tags
	* All non-object engine methods
	* RTB_registerPref with BLG options

![Syntax Highlighting](https://imgur.com/bnd7w5q.png)
![Autocomplete](https://imgur.com/1bDPSsd.gif)
![Autocomplete](https://imgur.com/GvudPbC.gif)
